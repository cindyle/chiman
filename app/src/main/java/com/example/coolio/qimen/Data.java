package com.example.coolio.qimen;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

public class Data {
    private Context context;
    //public String gua;   // 哪一宮
    public String god;   // 神
    public String sky;   // 天干
    public String ground;   // 地干
    public String parasite1;  // 天 寄干
    public String parasite2;  // 地 寄干
    public String star;    // 星
    public String door;    // 門
    public String doorp;    // 門迫
    public String oo;    // 空亡
    public String ingan;  // 引干
    public String skymo, groundmo, parasite1mo, parasite2mo;  // 入墓
    public String skygi, groundgi, parasite1gi, parasite2gi;  // 擊刑
    public String ho;     // 馬星


    public Boolean getSkymo() {
        if (skymo.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getGroundmo() {
        if (groundmo.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getOo() {
        if (oo.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getHo() {
        if (ho.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getParasite1mo() {
        if (parasite1mo.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getParasite2mo() {
        if (parasite2mo.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getSkygi() {
        if (skygi.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getGroundgi() {
        if (groundgi.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getParasite1gi() {
        if (parasite1gi.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getParasite2gi() {
        if (parasite2gi.equals("true")){
            return true;
        }else {
            return false;
        }
    }

    public Boolean getDoorp() {
        if (doorp.equals("true")){
            return true;
        }else {
            return false;
        }
    }



    public Data(Context ctx, JSONObject jo) {
        context = ctx;

        try {
            god = jo.getString("神");

            String skyobj = jo.getString("天盤干");
            JSONObject skyjo = new JSONObject(skyobj);
            sky = skyjo.getString("干");
            skymo = skyjo.getString("入墓");
            skygi = skyjo.getString("擊刑");

            String gboj = jo.getString("地盤干");
            JSONObject gjo = new JSONObject(gboj);
            ground = gjo.getString("干");
            groundmo = gjo.getString("入墓");
            groundgi = gjo.getString("擊刑");

            String skygiobj = jo.getString("天盤寄干");
            if (!skygiobj.equals("null")) {
                JSONObject skygijo = new JSONObject(skygiobj);
                parasite1 = skygijo.getString("干");
                parasite1mo = skygijo.getString("入墓");
                parasite1gi = skygijo.getString("擊刑");
            } else {
                parasite1 = "";
                parasite1mo = "";
                parasite1gi = "";
            }


            String ggi = jo.getString("地盤寄干");
            if (!ggi.equals("null")) {
                JSONObject ggiobj = new JSONObject(ggi);
                parasite2 = ggiobj.getString("干");
                parasite2mo = ggiobj.getString("入墓");
                parasite2gi = ggiobj.getString("擊刑");
            } else {
                parasite2 = "";
                parasite2mo = "";
                parasite2gi = "";
            }

            star = jo.getString("星");

            String doorobj = jo.getString("門");
            JSONObject doorjo = new JSONObject(doorobj);
            door = doorjo.getString("門");
            doorp = doorjo.getString("門迫");


            oo = jo.getString("空亡");

            String inganobj = jo.getString("引干");
            JSONObject inganjo = new JSONObject(inganobj);
            ingan = inganjo.getString("干");

            ho = jo.getString("馬");

            //System.out.println(god + " " + sky + "  " + ground + "  " + parasite1 + "  " + parasite2 + "   "
                   // + star + "  " + door + "  " + oo + "  " + ingan + "  " + ho);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}