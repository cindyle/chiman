package com.example.coolio.qimen;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private TextView text;
    private DatePicker date;
    private Button btn;
    private Spinner spinner;
    private String[] hours = {"0:00~0:59","1:00~1:59","2:00~2:59","3:00~3:59","4:00~4:59","5:00~5:59",
            "6:00~6:59","7:00~7:59","8:00~8:59","9:00~9:59","10:00~10:59","11:00~11:59","12:00~12:59",
            "13:00~13:59","14:00~14:59","15:00~15:59","16:00~16:59","17:00~17:59","18:00~18:59","19:00~19:59",
            "20:00~20:59", "21:00~21:59","22:00~22:59","23:00~23:59"};
    private int year, month, day, hour;
    public String cdate;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();

        ArrayAdapter<String> hourlist = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_spinner_dropdown_item, hours){

        };

        spinner.setAdapter(hourlist);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                hour = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                year = date.getYear();
                month = date.getMonth() + 1;
                day = date.getDayOfMonth();



                cdate = String.format("%02d", year) + "-" +  String.format("%02d", month) + "-" + String.format("%02d", day) + "-" + String.format("%02d", hour);

                Intent intent = new Intent(MainActivity.this, Divine2Activity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("year", year);
                bundle.putInt("month", month);
                bundle.putInt("day", day);
                bundle.putString("hour", hours[hour]);
                bundle.putString("time", cdate);     // yyyy-mm-dd-hh

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });





    }

    private void findViews(){
        date = findViewById(R.id.date);
        btn = findViewById(R.id.btn);
        spinner = findViewById(R.id.spinner);
    }
}
