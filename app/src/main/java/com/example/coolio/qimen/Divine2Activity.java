package com.example.coolio.qimen;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.example.libpaipan.陰盤奇門.陰盤奇門;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;


public class Divine2Activity extends AppCompatActivity {
    private TextView tvWeDay, tvChDay, tvFourTime,tvNotime, tvLeadler, test, tvGod, tvSky, tvGround, tvParasite1, tvParasite2, tvStar, tvDoor;
    private String hour, time, nextTime, json, weDay, chDay, isyan, fourTime, notime, leader1, leader2, god, sky, ground, parasite1, parasite2, star, door;
    private int year, month, day, num;
    private ArrayList<Data> datalist;
    private 陰盤奇門 paipan;
    private ArrayList<Boolean> ooList, hoList;
    private Button btn;
    private int newday, newHour;
    private Room8Fragment room8Fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_divine2);
        findViews();
        getBundle();
        getjson();

        test.setText(time);
        newday = day;
        btn.setOnClickListener(listener);


    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String bHour = time.substring(time.length()-2, time.length());

            newHour = Integer.valueOf(bHour);
            if (newHour < 22) {
                newHour = newHour +2;
            } else {
                newday = newday+1;
                newHour = 0;
            }

            time = String.format("%02d", year) + "-" +  String.format("%02d", month) + "-" + String.format("%02d", newday) + "-" + String.format("%02d", newHour);

            test.setText(time);
            getjson();

             //將nextday的資料傳至room8Fragment
            room8Fragment = new Room8Fragment();
            Bundle bundle = new Bundle();
            bundle.putString("nexttime", time);
            room8Fragment.setArguments(bundle);

            room8Fragment.nextPan();





        }
    };

    // 取得時間資料
    private void getBundle() {
        Bundle bundle = getIntent().getExtras();
        year = bundle.getInt("year");
        month = bundle.getInt("month");
        day = bundle.getInt("day");
        hour = bundle.getString("hour");
        time = bundle.getString("time");

    }

    private void getjson() {

        paipan = new 陰盤奇門(time);
        json = paipan.toString();   // 為輸出的json
        //System.out.println(json);
        parseJson(json);
    }
    private void parseJson(String s) {
        try {
            JSONObject obj = new JSONObject(s);
            JSONArray room_8 = obj.getJSONArray("宮"); // 8宮的json
            datalist = new ArrayList<>();
            for (int i = 0; i < room_8.length(); i++) {
                JSONObject jo = room_8.getJSONObject(i);
                Data data = new Data(Divine2Activity.this, jo);
                datalist.add(data);
//                Boolean hodata = data.getHo();
//                hoList.add(hodata);
//                Boolean oodata = data.getOo();
//                ooList.add(oodata);
            }

            notime = obj.getString("旬首");
            String head = obj.getString("符頭");  // layout還未用到
            leader1 = obj.getString("值符");
            leader2 = obj.getString("值使");
            num = obj.getInt("局數");
            JSONObject dayobj = obj.getJSONObject("日期");
            isyan = obj.getString("陽盤").substring(0,1);      // 陽遁or陰遁
            weDay = dayobj.getString("陽曆");
            chDay = dayobj.getString("農曆");
            String chday_y = chDay.substring(0,4);
            String chday_m = chDay.substring(5,7);
            String chday_d = chDay.substring(8,10);
            fourTime = dayobj.getString("四柱");



            // 畫面填值
            String time_y = time.substring(0,4);
            String time_m = time.substring(5,7);
            String time_d = time.substring(8,10);
            tvWeDay.setText(String.format(getString(R.string.weday), time_y, time_m, time_d, hour));          // 西元日期
            tvChDay.setText(String.format(getString(R.string.chday), chday_y, chday_m, chday_d, isyan, num));   // 農曆日期
            tvFourTime.setText(String.format(getString(R.string.four_time), fourTime));     // 四柱
            tvNotime.setText(String.format(getString(R.string.no_time),notime));            // 空亡、馬星（馬星未設）
            tvLeadler.setText(String.format(getString(R.string.leader), leader1, leader2)); // 值符、值使


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void findViews(){

        //recyclerView = findViewById(R.id.recycler);
        tvWeDay = findViewById(R.id.weDay);
        tvChDay = findViewById(R.id.chDay);
        tvGod = findViewById(R.id.god);
        tvSky= findViewById(R.id.sky);
        tvGround = findViewById(R.id.ground);
        tvParasite1 = findViewById(R.id.parasite1);
        tvParasite2 = findViewById(R.id.parasite2);
        tvStar = findViewById(R.id.star);
        tvDoor = findViewById(R.id.door);
        tvLeadler = findViewById(R.id.leaderTv);
        tvFourTime = findViewById(R.id.fourTime);
        tvNotime = findViewById(R.id.noTime);
        btn = findViewById(R.id.btn);
        test = findViewById(R.id.test);

    }

}
