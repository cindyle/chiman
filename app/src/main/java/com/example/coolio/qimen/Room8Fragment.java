package com.example.coolio.qimen;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.libpaipan.陰盤奇門.陰盤奇門;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Room8Fragment extends Fragment {
    private RecyclerView recyclerView;
    private TextView etv1, etv2, etv3, etv4, etv5, etv6, etv7, etv8;
    private TextView ho1, ho2, ho3,ho4;
    private String hour, time, json, nexttime, etv, weDay, chDay, isyan, fourTime, notime, leader1, leader2, god, sky, ground, parasite1, parasite2, star, door;
    private int year, month, day, num;
    private ArrayList<Data> datalist, finalData;
    private ArrayList <String> inganList;
    private ArrayList<Boolean> hoList, doorpList, skymoList, skygiList, groundmoList, groundgiList, parasite1moList, parasite2moList, parasite1giList, parasite2giList;
    private 陰盤奇門 paipan;
    private Data data;
    private Bundle bundle;
    private RoomAdatper adapter;

    public Room8Fragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_room8, container, false);
        findViews(view);
        getBundle();
        getjson(time);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        adapter = new RoomAdatper(finalData);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));



        return view;
    }




    private void findViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView2);
        ho1 = view.findViewById(R.id.ho1);
        ho2 = view.findViewById(R.id.ho2);
        ho3 = view.findViewById(R.id.ho3);
        ho4 = view.findViewById(R.id.ho4);
        etv1 = view.findViewById(R.id.etv1);
        etv2 = view.findViewById(R.id.etv2);
        etv3 = view.findViewById(R.id.etv3);
        etv4 = view.findViewById(R.id.etv4);
        etv5 = view.findViewById(R.id.etv5);
        etv6 = view.findViewById(R.id.etv6);
        etv7 = view.findViewById(R.id.etv7);
        etv8 = view.findViewById(R.id.etv8);
    }

    private void setViews(){
        // 設引干
        System.out.println(inganList);

        etv1.setText(inganList.get(0));
        etv2.setText(inganList.get(6));
        etv3.setText(inganList.get(2));
        etv4.setText(inganList.get(3));
        etv5.setText(inganList.get(7));
        etv6.setText(inganList.get(1));
        etv7.setText(inganList.get(5));
        etv8.setText(inganList.get(4));

        // 設馬星
        if (hoList.get(3) == true){
            ho1.setText("馬");
        } else if (hoList.get(1) == true){
            ho2.setText("馬");
        }else if (hoList.get(6) == true){
            ho3.setText("馬");
        }else if (hoList.get(4) == true){
            ho4.setText("馬");
        }


    }

    // 取得時間資料
    private void getBundle() {
        Bundle bundle = getActivity().getIntent().getExtras();
        year = bundle.getInt("year");
        month = bundle.getInt("month");
        day = bundle.getInt("day");
        hour = bundle.getString("hour");
        time = bundle.getString("time");
    }

    public void nextPan (){
        // 接收activity"下一盤"按鍵訊息
        bundle = this.getArguments();
        if (bundle != null){
            nexttime = bundle.getString("nexttime");
            paipan = new 陰盤奇門(nexttime);
            json = paipan.toString();
            parseJson(json);
            adapter = new RoomAdatper(finalData);
            recyclerView.setAdapter(adapter);


        }
    }

    private void getjson(String jsonTime){
        jsonTime = this.time;
        paipan = new 陰盤奇門(jsonTime);
        json = paipan.toString();   // 為輸出的json
        parseJson(json);
        System.out.println(json);

    }


    private void parseJson(String s) {
        try {
            JSONObject obj = new JSONObject(s);
            JSONArray room_8 = obj.getJSONArray("宮"); // 8宮的json
            datalist = new ArrayList<>();
            inganList = new ArrayList<>();
            hoList = new ArrayList<>();
            doorpList = new ArrayList<>();
            skymoList = new ArrayList<>();
            groundmoList = new ArrayList<>();
            parasite1moList = new ArrayList<>();
            parasite2moList = new ArrayList<>();
            skygiList = new ArrayList<>();
            groundgiList = new ArrayList<>();
            parasite1giList = new ArrayList<>();
            parasite2giList = new ArrayList<>();
            for (int i = 0; i < room_8.length(); i++) {
                JSONObject jo = room_8.getJSONObject(i);
                Data data = new Data(getActivity(), jo);
                datalist.add(data);
                String ingandata = data.ingan;
                inganList.add(ingandata);
                Boolean hodata = data.getHo();
                hoList.add(hodata);
                Boolean doorpdata = data.getDoorp();
                doorpList.add(doorpdata);
                Boolean skymodata = data.getSkymo();
                skymoList.add(skymodata);
                Boolean skygidata = data.getSkygi();
                skygiList.add(skygidata);

                Boolean groundmodata = data.getGroundmo();
                groundmoList.add(groundmodata);
                Boolean groundgidata = data.getGroundgi();
                groundgiList.add(groundgidata);

                Boolean parasite1modata = data.getParasite1mo();
                parasite1moList.add(parasite1modata);
                Boolean parasite1gidata = data.getParasite1gi();
                parasite1giList.add(parasite1gidata);

                Boolean parasite2modata = data.getParasite2mo();
                parasite2moList.add(parasite2modata);
                Boolean parasite2gidata = data.getParasite2gi();
                parasite2giList.add(parasite2gidata);
            }


            finalData = new ArrayList();
            finalData.add(datalist.get(3));
            finalData.add(datalist.get(7));
            finalData.add(datalist.get(1));
            finalData.add(datalist.get(2));
            finalData.add(datalist.get(3));
            finalData.add(datalist.get(5));
            finalData.add(datalist.get(6));
            finalData.add(datalist.get(0));
            finalData.add(datalist.get(4));







            notime = obj.getString("旬首");
            String head = obj.getString("符頭");  // layout還未用到
            leader1 = obj.getString("值符");
            leader2 = obj.getString("值使");
            num = obj.getInt("局數");
            JSONObject dayobj = obj.getJSONObject("日期");
            isyan = obj.getString("陽盤");
            weDay = dayobj.getString("陽曆");
            chDay = dayobj.getString("農曆");
            fourTime = dayobj.getString("四柱");

            setViews();


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
