package com.example.coolio.qimen;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RoomAdatper extends RecyclerView.Adapter < RoomAdatper.ViewHolder> {
    private Context context;
    private Data data;
    private ArrayList<Data> datalist;

    public RoomAdatper(ArrayList<Data> dataList) {
        this.datalist = dataList;
    }


    @NonNull
    @Override
    public RoomAdatper.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_item, viewGroup, false);
        return new RoomAdatper.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomAdatper.ViewHolder viewHolder, int i) {
        data = datalist.get(i);
        if (i==4) {
            viewHolder.setnoData(data);
        }else {

            viewHolder.setData(data);
            //holder.itemView.setTag(data);      // 做一個標籤，未來點擊時才能抓到這個item
        }

        if (i == 2){
            if (data.god.equals("值符")){
                viewHolder.setColor(data);
            }
        }

    }

    @Override
    public int getItemCount() {
        return 9;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvGod, tvSky, tvGround, tvParasite1, tvParasite2, tvStar, tvDoor;
        private ImageView tvoo;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvGod = itemView.findViewById(R.id.god);
            tvSky = itemView.findViewById(R.id.sky);
            tvGround = itemView.findViewById(R.id.ground);
            tvParasite1 = itemView.findViewById(R.id.parasite1);
            tvParasite2 = itemView.findViewById(R.id.parasite2);
            tvStar = itemView.findViewById(R.id.star);
            tvDoor = itemView.findViewById(R.id.door);
            tvoo = itemView.findViewById(R.id.tvoo);

            itemView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        }

        public void setData(Data data) {

            tvGod.setText(data.god);
            tvSky.setText(data.sky);
            tvGround.setText(data.ground);
            tvParasite1.setText(data.parasite1);
            tvParasite2.setText(data.parasite2);
            tvStar.setText(data.star);
            tvDoor.setText(data.door);

            if (data.oo.equals("true")) {
                tvoo.setVisibility(View.VISIBLE);
            } else {
                tvoo.setVisibility(View.GONE);
            }

            // 設門迫
            if (data.getDoorp() == true){
                tvDoor.setTextColor(android.graphics.Color.RED);
            }

            // 設擊刑
            if (data.getSkygi() == true){
                tvSky.setTextColor(android.graphics.Color.RED);
            }

            if (data.getGroundgi() == true){
                tvGround.setTextColor(android.graphics.Color.RED);
            }

            if (data.getParasite1gi() == true){
                tvParasite1.setTextColor(android.graphics.Color.RED);
            }

            if (data.getParasite2gi() == true){
                tvParasite2.setTextColor(android.graphics.Color.RED);
            }

            // 設入墓
            if (data.getSkymo() == true){
                tvSky.setTextColor(android.graphics.Color.BLUE);
            }

            if (data.getGroundmo() == true){
                tvGround.setTextColor(android.graphics.Color.BLUE);
            }

            if (data.getParasite1mo() == true){
                tvParasite1.setTextColor(android.graphics.Color.BLUE);
            }

            if (data.getParasite2mo() == true){
                tvParasite2.setTextColor(android.graphics.Color.BLUE);
            }



        }

        public void setnoData(Data data) {
            tvGod.setVisibility(View.GONE);
            tvSky.setVisibility(View.GONE);
            tvGround.setVisibility(View.GONE);
            tvParasite1.setVisibility(View.GONE);
            tvParasite2.setVisibility(View.GONE);
            tvStar.setVisibility(View.GONE);
            tvDoor.setVisibility(View.GONE);
            tvoo.setVisibility(View.GONE);
        }

        public void setColor(Data data) {
            tvGod.setTextColor(android.graphics.Color.BLUE);
        }

    }



}
